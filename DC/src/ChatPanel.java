import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Vector;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author Javonne Martin 16763777
 */
public class ChatPanel extends javax.swing.JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Creates new form ChatPanel
	 */
	Client c;

	public ChatPanel(Client c) {
		initComponents();
		this.c = c;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		cpInput = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		cpChatfield = new javax.swing.JTextArea();
		jScrollPane2 = new javax.swing.JScrollPane();
		cpContacts = new javax.swing.JTextArea();
		cpChat = new javax.swing.JLabel();
		cpOnlinetext = new javax.swing.JLabel();
		cpDisconnect = new javax.swing.JButton();
		PauseButton = new javax.swing.JButton();
		acceptKeys = new javax.swing.JComboBox();
		accept = new javax.swing.JButton();
		decline = new javax.swing.JButton();
		jLabel2 = new javax.swing.JLabel();
		jProgressBar1 = new javax.swing.JProgressBar();
		jProgressBar2 = new javax.swing.JProgressBar();

		cpInput.setToolTipText("Enter Message");

		cpChatfield.setColumns(20);
		cpChatfield.setRows(5);
		jScrollPane1.setViewportView(cpChatfield);

		cpContacts.setColumns(20);
		cpContacts.setRows(5);
		jScrollPane2.setViewportView(cpContacts);

		cpChat.setText("Chat Area");

		decline.setEnabled(false);
		cpOnlinetext.setText("Online Contacts");
		cpInput.addKeyListener(new java.awt.event.KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				cpEnterActionPerformed(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}

		});
		cpDisconnect.setText("Exit");
		cpDisconnect.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cpDisconnectActionPerformed(evt);
			}
		});

		PauseButton.setText("Pause");
		PauseButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				pauseFile();//Calling here
			}
		});

		acceptKeys.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
				"Item 1", "Item 2", "Item 3", "Item 4" }));
		acceptKeys.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				
			}
		});

		accept.setText("Upload");
		accept.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				//Get key from list and call accept file method
				String temp = acceptKeys.getSelectedItem().toString();
				String[] splitOption = temp.split(" ");
				c.accept_File(Integer.parseInt(splitOption[0]));
			}
		});

		decline.setText("Decline");
		decline.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			
			}
		});

		jLabel2.setText("Voice Options");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(223, 223, 223)
								.addComponent(cpChat)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(cpOnlinetext).addGap(77, 77, 77))
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		cpInput,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		667,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGap(18, 18,
																		18)
																.addComponent(
																		cpDisconnect,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		jScrollPane1,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		521,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addGroup(
																						javax.swing.GroupLayout.Alignment.TRAILING,
																						layout.createSequentialGroup()
																								.addPreferredGap(
																										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																										javax.swing.GroupLayout.DEFAULT_SIZE,
																										Short.MAX_VALUE)
																								.addComponent(
																										jLabel2)
																								.addGap(87,
																										87,
																										87))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addPreferredGap(
																										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(
																										layout.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.LEADING)
																												.addComponent(
																														jScrollPane2,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														269,
																														Short.MAX_VALUE)
																												.addGroup(
																														layout.createSequentialGroup()
																																.addGroup(
																																		layout.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.LEADING,
																																				false)
																																				.addComponent(
																																						PauseButton,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						Short.MAX_VALUE)
																																				.addComponent(
																																						accept,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						150,
																																						Short.MAX_VALUE))
																																.addPreferredGap(
																																		javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																																.addGroup(
																																		layout.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.LEADING)
																																				.addComponent(
																																						acceptKeys,
																																						0,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						Short.MAX_VALUE)
																																				.addComponent(
																																						decline,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						Short.MAX_VALUE)))
																												.addComponent(
																														jProgressBar1,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														Short.MAX_VALUE)
																												.addComponent(
																														jProgressBar2,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														Short.MAX_VALUE))))))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(30, 30, 30)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(cpChat)
												.addComponent(cpOnlinetext))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(
														jScrollPane1,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														382,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		jScrollPane2,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		232,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jLabel2)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						PauseButton)
																				.addComponent(
																						acceptKeys,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						accept)
																				.addComponent(
																						decline))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jProgressBar1,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jProgressBar2,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addGap(18, 18, Short.MAX_VALUE)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														cpInput,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(cpDisconnect))
								.addContainerGap(23, Short.MAX_VALUE)));
	}// </editor-fold>//GEN-END:initComponents

	private void cpEnterActionPerformed(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_cpDisconnectActionPerformed
		if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
			if (c.get_conStats() == false) {
				if (cpInput.getText().equals("\\disconnect")) {
					try {
						c.Disconnect();
					} catch (IOException e) {
						System.exit(0);
						e.printStackTrace();
					}
				}
				return;
			}
			String input = cpInput.getText();
			String[] inputs = input.split(";");

			if (inputs[0].equals("\\users")) {

				c.sendMessage(new Packet(Packet_Type.COMMAND,
						Packet_Subtype.USER_LIST, c.username, null, null));

				System.out.println("User list");
			} else if (inputs[0].equals("\\exit")) {
				c.sendMessage(new Packet(Packet_Type.CONNECTION,
						Packet_Subtype.USER_DISCONNECTED, c.username, null,
						null));
				System.out.println("Exit");
			} else if (inputs[0].equals("\\shared")) {
				System.out.println("Your shhared files:");
				c.displayfiles();
				System.out.println("------------------------------------");
			} else if (inputs[0].equals("\\currentsearch")) {
				c.printsearch();

			} else if (inputs.length >= 2) {
				System.out.println("second");
				if (inputs[0].equals("\\find")) {
					c.search(inputs[1]);
				} else if (inputs[0].equals("\\accept")) {
					try {
						int id = Integer.parseInt(inputs[1]);
						c.accept_File(id);
					} catch (Exception e) {
						add_message("Parameter is not a number");
					}

				} else if (inputs.length > 2 && inputs[0].equals("\\reqfile")) {
					c.reqFile(inputs[1], inputs[2]);
					System.out.println("req");
				} else if (inputs[0].equals("\\whisper")) {

					c.sendMessage(new Packet(Packet_Type.MESSAGE,
							Packet_Subtype.WHISPER, c.username, inputs[1],
							inputs[2]));

					System.out.println("whisper");
				} else {
					c.sendMessage(new Packet(Packet_Type.MESSAGE,
							Packet_Subtype.ALL, c.username, null, input));
				}
			} else {
				c.sendMessage(new Packet(Packet_Type.MESSAGE,
						Packet_Subtype.ALL, c.username, null, input));
			}
			cpInput.setText("");

		}
	}

	public void pauseFile() {
		if (PauseButton.getText().equals("Pause")) {
			PauseButton.setText("Resume");
			c.Pause();
		} else if (PauseButton.getText().equals("Resume")) {
			PauseButton.setText("Pause");
			c.Resume();
		}
	}

	/**
	 * Listener on the Exit button to disconnect the server
	 * 
	 * @param evt
	 */
	private void cpDisconnectActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cpDisconnectActionPerformed
		if (cpDisconnect.getText().equals("Send Voice")) {
		} else {

		}

	}// GEN-LAST:event_cpDisconnectActionPerformed

	public javax.swing.JComboBox get_ComboBox() {
		return acceptKeys;
	}

	/**
	 * Returns th chat field of the Panel
	 * 
	 * @return cpChatfield
	 */
	public javax.swing.JTextArea get_ContactsField() {
		return cpContacts;
	}

	/**
	 * Returns the Contacts field of the Panel
	 * 
	 * @return cpContacts
	 */
	public javax.swing.JTextArea get_ChatField() {
		return cpChatfield;
	}

	public javax.swing.JTextField get_input() {
		return cpInput;
	}

	public void add_message(String msg) {
		cpChatfield.setText(cpChatfield.getText() + '\n' + msg);

	}

	/**
	 * Updates the contacts panel
	 * 
	 * @param msg
	 */
	public void update_Contacts(String msg) {
		cpContacts.setText(msg);

		
	}
	public void update_list(String msg){
		comboVector.add(msg);
		acceptKeys.setModel(new javax.swing.DefaultComboBoxModel(comboVector));
	}

	public void update_progress(double n) {
		jProgressBar1.setValue((int) (n * 100));
	}
	public void update_progressU(double n){
		jProgressBar2.setValue((int)(n*100));
	}

	/**
	 * Sets the buttons such that the call is established
	 */

	private Vector<String> comboVector = new Vector<String>();
	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JLabel cpChat;
	private javax.swing.JTextArea cpChatfield;
	private javax.swing.JTextArea cpContacts;
	private javax.swing.JButton cpDisconnect;
	private javax.swing.JTextField cpInput;
	private javax.swing.JLabel cpOnlinetext;
	private javax.swing.JButton PauseButton;
	private javax.swing.JButton accept;
	private javax.swing.JButton decline;
	private javax.swing.JComboBox acceptKeys;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JProgressBar jProgressBar1;
	private javax.swing.JProgressBar jProgressBar2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	// End of variables declaration//GEN-END:variables
}
