import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Client {
	Socket socket;
	ObjectOutputStream out;
	ObjectInputStream in;
	Thread Userinput;
	Thread Packet_in;
	Thread TCPoutThread;
	Boolean resolved = false;
	boolean online = false;
	String username = null;
	final int BUFFERSIZE = 8000;
	List<Packet> tcpBuffer;

	boolean pauseStatus;
	boolean pauseStatusUpload;
	boolean pauseStatisDownload;
	Boolean uploading_inprogress = false;
	Boolean downloading_inprogress = false;
	String DownloadEndPoint = null;
	String UploadEndPoint = null;

	int searchTag = 0;
	int downloadKey = 0;
	int KeyGen = 0;
	Hashtable<Integer, LinkedList<String>> reqlist;
	Hashtable<Integer, LinkedList<String>> downloadlist;
	Hashtable<String, LinkedList<String>> LastKnownFiles;
	Hashtable<String, String> sharedFiles;

	Hashtable<Integer, SendingFile> req;
	Hashtable<Integer, ReceivingFile> down;
	ReceivingFile InFilethread;
	SendingFile OutFilethread;

	String SharedFolder = "Shared/";
	String UploadFolder = "Downloading/";

	ClientEntry ce;
	ChatPanel cp;
	Login lp;
	Username up;
	Thread out2;

	/**
	 * Constructor for the client class
	 * 
	 * @param ce
	 */
	public Client(ClientEntry ce) {
		pauseStatus = false;
		LastKnownFiles = new Hashtable<String, LinkedList<String>>();
		sharedFiles = new Hashtable<String, String>();
		reqlist = new Hashtable<Integer, LinkedList<String>>();
		downloadlist = new Hashtable<Integer, LinkedList<String>>();
		req = new Hashtable<Integer, SendingFile>();
		down = new Hashtable<Integer, ReceivingFile>();
		updateLocalFiles();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				if (socket != null) {
					if (socket.isConnected()) {
						if (out != null) {
							try {
								synchronized (out) {
									out.writeObject(new Packet(
											Packet_Type.CONNECTION,
											Packet_Subtype.USER_DISCONNECTED,
											username, null, null));
									out.flush();
									System.out.println("Send logout");
								}
							} catch (Exception e) {

							}
						}
					}
					if (!socket.isClosed()) {
						System.out.println("Closing socket");
						try {
							socket.close();
						} catch (IOException e) {

						}
					}
				}
			}
		});
		this.ce = ce;

		tcpBuffer = Collections.synchronizedList(new LinkedList<Packet>());

		online = true;

		Packet_in = new Thread(new Packet_Listener());

		TCPoutThread = new Thread(new TCPOut());

	}

	/**
	 * Initialises variables for the GUI
	 */
	public void initGUI() {
		cp = ce.get_chatpanel();
		lp = ce.get_login();
		up = ce.get_userpanel();
	}

	/**
	 * Connects to the Server in a separate thread
	 * 
	 * @param ip
	 */
	public void Connect(String ip) {
		if (ip == null) {
			return;
		}
		if (ip.equals("")) {
			return;
		}
		System.out.println(ip);
		Thread t = new Thread(new connection(ip));
		t.start();
	}

	/**
	 * Attempts to login using the username provide in a separate thread
	 * 
	 * @param uname
	 */
	public void Login(String uname) {

		if (uname == null) {
			return;
		}
		System.out.println(uname);
		Thread t = new Thread(new loginThread(uname));
		t.start();
	}

	/**
	 * Sends a packet
	 * 
	 * @param p
	 */
	public void sendMessage(Packet p) {
		synchronized (tcpBuffer) {
			tcpBuffer.add(p);
		}
	}

	/**
	 * gets the connection status
	 * 
	 * @return boolean
	 */
	public boolean get_conStats() {
		return online;
	}

	/**
	 * closes and disconnects from the server
	 * 
	 * @throws IOException
	 */
	public void Disconnect() throws IOException {
		socket.close();
		System.exit(0);
	}

	public void Pause() {
		System.out.println("--Setting pauseStatus to true");
		// TODO send packet
		pauseStatus = true;
		// Must instead pause download/upload boolean
	}

	public void Resume() {
		System.out.println("--Setting pauseStatus to false");
		// TODO send packet
		pauseStatus = false;
		// Must instead pause download/upload boolean
	}

	/**
	 * Sets variables to accept call
	 */
	public void accept_File(int id) {
		System.out.println("Sending accept and udp socket\n");
		if (reqlist.containsKey(id)) {
			LinkedList<String> list = reqlist.get(id);
			synchronized (uploading_inprogress) {
				uploading_inprogress = true; // TODO this thing is dynamic
												// enought that this isn't
												// necessary but due to blocked
												// ports we may need to use a
												// variable like this
			}
			Packet p = new Packet(Packet_Type.FSP, Packet_Subtype.ACCEPTREQ,
					username, list.get(0), null);
			p.set_integer(id);
			sendMessage(p);
			Packet pk = new Packet(Packet_Type.FSP, Packet_Subtype.EXCHANGEREQ,
					username, list.get(0), "");
			pk.set_integer(id);
			cp.add_message("Accepting the file request, " + id + ":"
					+ list.get(0) + ":" + list.get(1));
			sendMessage(pk);

			OutFilethread = new SendingFile(list.get(0), list.get(1), id);
			OutFilethread.start();
			req.put(id, OutFilethread);

		} else {
			cp.add_message("No request waiting with that ID");
		}
	}

	/**
	 * 
	 * @param file
	 */
	public void search(String file) {
		Packet p = new Packet(Packet_Type.FSP, Packet_Subtype.REQSEARCH,
				username, null, file);
		p.set_integer(searchTag);
		sendMessage(p);

		searchTag++;
		LastKnownFiles = new Hashtable<String, LinkedList<String>>();

	}

	/**
	 * 
	 * @param user
	 * @param file
	 */
	public void reqFile(String user, String file) {

		System.out.println("REQUESTING");
		LinkedList<String> l = new LinkedList<String>();
		generateUpKey();
		l.add(user);
		l.add(file);
		downloadlist.put(KeyGen, l);

		Packet p = new Packet(Packet_Type.FSP, Packet_Subtype.REQFILE,
				username, user, file);
		p.set_integer(KeyGen);
		sendMessage(p);
		cp.add_message("Sent request for file, " + file
				+ " to user. Using upload Key " + KeyGen);

	}

	public void updateBox() {
		StringBuilder sb = new StringBuilder();
		synchronized (req) {
			Iterator<Integer> it = req.keySet().iterator();
			while (it.hasNext()) {
				Integer key = it.next();
				SendingFile sf = req.get(key);
				sb.append(key + " " + sf.filename + " Upload");
				sb.append("\n");
			}
		}
		synchronized (down) {
			Iterator<Integer> it = down.keySet().iterator();
			System.out.println();
			while (it.hasNext()) {
				Integer key = it.next();
				SendingFile sf = req.get(key);
				System.out.println(key + ":" + sf);
				if (sf != null) {
					sb.append(key + " " + sf.filename + " Download");
					sb.append("\n");
				}
			}
		}
		// cp.update_list(sb.toString());
	}

	/**
	 * 
	 */
	public void updateLocalFiles() {
		File folder = new File(SharedFolder);
		String[] files = folder.list();
		if (files != null) {
			for (String k : files) {
				System.out.println(k);
				sharedFiles.put(k, "");
			}
		}
	}

	/**
	 * 
	 * @param user
	 * @param files
	 */
	public void insertfiles(String user, String files) {
		String[] f = files.split("\\n");
		for (String cf : f) {
			if (LastKnownFiles.containsKey(cf)) {
				LastKnownFiles.get(cf).add(user);
			} else {
				LinkedList<String> users = new LinkedList<String>();
				users.add(user);
				LastKnownFiles.put(cf, users);
			}
		}
	}

	/**
	 * 
	 */
	public void printsearch() {

		cp.add_message("NOT IMPLEMENTED");
	}

	/**
	 * 
	 */
	public void displayfiles() {
		Iterator<String> keys = sharedFiles.keySet().iterator();
		StringBuilder sb = new StringBuilder();
		while (keys.hasNext()) {
			sb.append(keys.next());
			sb.append("\n");
		}
		cp.add_message(sb.toString());
	}

	public void generateUpKey() {

		System.out.println("METHOD " + reqlist.containsKey(KeyGen));
		while (reqlist.containsKey(KeyGen) || downloadlist.containsKey(KeyGen)) {
			KeyGen = new Integer((int) (Math.random() * 255));
			System.out.println(KeyGen);
		}
	}

	//
	/**
	 * Declines a call
	 */
	public void decline_call() {
		sendMessage(new Packet(Packet_Type.FSP, Packet_Subtype.DECLINEREQ,
				username, DownloadEndPoint, null));

	}

	/**
	 * Hangs up a call in progress
	 */
	public void end_call() {
		sendMessage(new Packet(Packet_Type.FSP, Packet_Subtype.CLOSE, username,
				DownloadEndPoint, null));
	}

	/**
	 * Thread used for connecting to a server
	 * 
	 * @author 16763777
	 * 
	 */
	class connection implements Runnable {
		String ip;

		public connection(String ip) {
			this.ip = ip;
		}

		public void run() {
			try {
				socket = new Socket(ip, 3000);
				System.out.println("coNnected");
				out = new ObjectOutputStream(socket.getOutputStream());
				System.out.println("Stream");
				in = new ObjectInputStream((socket.getInputStream()));
				System.out.println("Stream");
				ce.set_UsernameScreen();
			} catch (SocketException e) {

				lp.lOutput.setText("Socket Execption");
			} catch (UnknownHostException e) {
				lp.lOutput.setText("Unknown Host");

			} catch (IOException e) {
				lp.lOutput.setText("IO error");

			}

		}
	}

	/**
	 * Thread used for connecting to a server
	 * 
	 * @author 16763777
	 * 
	 */
	class loginThread implements Runnable {
		String input;

		public loginThread(String u) {
			input = u;
		}

		public void run() {

			boolean wait = false;

			while (true) {
				if (!wait) {
					try {
						System.out.println("Sent");
						out.writeObject(new Packet(Packet_Type.CONNECTION,
								Packet_Subtype.VALIDATE_NAME, null, input, null));
						out.flush();
					} catch (IOException e) {
						up.uOutput.setText("Sending Error");
					}

					wait = true;
					System.out.println("Sent name");

				} else {
					Packet p;
					System.out.println("Else block");
					try {
						if ((p = (Packet) in.readObject()) != null) {
							System.out.println("MSG read");
							if (p.get_packet_type() == Packet_Type.CONNECTION
									&& p.get_packet_subtype() == Packet_Subtype.VALIDATE_NAME
									&& p.get_message().equals("true")) {
								Packet_in.start();
								TCPoutThread.start();
								username = p.get_parameters();
								System.out.println("Accepted");
								ce.set_ChatScreen();
								break;
							} else if (p.get_message().equals("false")) {
								wait = false;
								up.uOutput.setText("Username in use");
								break;
							} else {
								System.out.println("11111111");
								up.uOutput.setText("General Error");
								break;

							}

						} else {
							System.out.println("NULL msg");
						}
					} catch (IOException e) {

						e.printStackTrace();
					} catch (ClassNotFoundException e) {

						e.printStackTrace();
					}
				}
			}

		}

	}

	/**
	 * Sends all messages over TCP in the Buffer
	 * 
	 * @author 16763777
	 * 
	 */
	class TCPOut implements Runnable {
		public void run() {
			while (online) {
				synchronized (tcpBuffer) {
					if (tcpBuffer.size() > 0) {
						try {
							out.writeObject(tcpBuffer.remove(0));
						} catch (IOException e) {

							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Listens to incoming messages on the tcp port
	 * 
	 * @author 16763777
	 * 
	 */
	class Packet_Listener implements Runnable {
		public void run() {
			Packet msg;
			while (online) {
				try {
					if ((msg = (Packet) in.readObject()) != null) {
						switch (msg.get_packet_type()) {
						case CONNECTION:
							switch (msg.get_packet_subtype()) {
							case VALIDATE_NAME:
								System.out.println("VALIDATE");
								break;
							case FAILED:
								System.out.println("FAILED");
								break;
							case USER_JOINED:
								System.out.println(msg.get_parameters() + " @ "
										+ msg.get_date() + "Joined the Server");
								cp.add_message(msg.get_parameters() + " @ "
										+ msg.get_date() + "Joined the Server");

								break;
							default:
								System.out.println("\t inner EROROROROROR");
							}
							break;
						case COMMAND:
							System.out.println("Command");
							switch (msg.get_packet_subtype()) {
							case USER_LIST:
								cp.update_Contacts(msg.get_message());
								System.out.println(msg.get_message());
								break;
							}
							break;
						case MESSAGE:
							System.out.println("Message");
							switch (msg.get_packet_subtype()) {
							case ALL:
								cp.add_message(msg.get_username() + " "
										+ msg.get_date() + " : "
										+ msg.get_message());
								break;
							case WHISPER:
								if (msg.get_username().equals(username)) {
									cp.add_message("Whisper to, "
											+ msg.get_parameters() + " "
											+ msg.get_date() + " : "
											+ msg.get_message());
								} else {
									cp.add_message("Whisper from, "
											+ msg.get_username() + " "
											+ msg.get_date() + " : "
											+ msg.get_message());
								}
								break;
							case FAILED:
								System.out.println("FAILED");
								cp.add_message(msg.get_message());
								break;
							}
							break;
						case FSP:
							switch (msg.get_packet_subtype()) {
							case REQFILE:
								synchronized (uploading_inprogress) {

									if (uploading_inprogress) {
										// TODO auto reject if in call
										cp.add_message("This is the boolean");
										cp.add_message("Request FAILED from "
												+ msg.get_username()
												+ " for file, "
												+ msg.get_message());
										Packet p = new Packet(Packet_Type.FSP,
												Packet_Subtype.FAILED,
												username, msg.get_username(),
												"REQ");
										p.set_integer(msg.get_integer());
										sendMessage(p);
									} else {
										if (sharedFiles.containsKey(msg
												.get_message())) {
											cp.add_message("Request from "
													+ msg.get_username()
													+ " for file, "
													+ msg.get_message()
													+ " type \\accept;"
													+ msg.get_integer()
													+ " to accept the message");
											String temp = msg.get_integer()
													+ " " + msg.get_message();
											cp.update_list(temp);
											UploadEndPoint = msg.get_username();
											LinkedList<String> list = new LinkedList<String>();
											list.add(msg.get_username());
											list.add(msg.get_message());
											reqlist.put(msg.get_integer(), list);
											System.out.println("POESTED");
											System.out.println(req);

											System.out.println("POESTED");
											// cp.update_Contacts(msg)

										} else {
											cp.add_message("Request FAILED from "
													+ msg.get_username()
													+ " for file, "
													+ msg.get_message());
											Packet p = new Packet(
													Packet_Type.FSP,
													Packet_Subtype.FAILED,
													username,
													msg.get_username(), "REQ");
											p.set_integer(msg.get_integer());
											sendMessage(p);
										}
									}

								}
								break;
							case CLOSE:
								cp.add_message("Call ended "
										+ msg.get_username() + " "
										+ msg.get_parameters());
								System.out.println("Call ended "
										+ msg.get_username() + " "
										+ msg.get_parameters());
								synchronized (uploading_inprogress) {

									DownloadEndPoint = null;
									uploading_inprogress = false;
									// cp.end_call();

								}
								break;
							case DECLINEREQ:
								System.out.println(msg.get_username()
										+ " Declined the chat");
								cp.add_message(msg.get_username()
										+ " Declined the chat");
								synchronized (uploading_inprogress) {
									DownloadEndPoint = null;
									uploading_inprogress = false;
									// cp.end_call();

								}
								break;
							case ACCEPTREQ:
								System.out.println(msg.get_username()
										+ " Accepted the chat");
								cp.add_message(msg.get_username()
										+ " Accepted the chat");
								synchronized (uploading_inprogress) {
									uploading_inprogress = true;
									DownloadEndPoint = msg.get_username();
								}

								synchronized (tcpBuffer) {
									Packet pk = new Packet(Packet_Type.FSP,
											Packet_Subtype.EXCHANGEREQ,
											username, msg.get_username(),
											"download");
									tcpBuffer.add(pk);
									System.out
											.println("Sent UPD SOCKET for input");
								}
								break;
							case EXCHANGEREQ:
								// cp.call_established();
								System.out
										.println("Binding the UDP and starting threads");

								if (msg.get_message().equals("download")) { // start
																			// a
																			// upload
																			// thread
																			// using
																			// the
																			// other
																			// clients
																			// download
																			// ports
																			// System.out.println("uploading");
									// System.out.println(reqlist.get(msg
									// .get_integer()));
									// LinkedList<String> list = reqlist.get(msg
									// .get_integer());
									// OutFilethread = new Thread(new
									// SendingFile(
									// list.get(0), list.get(1)
									// ));
									// OutFilethread.start();
								} else { // start a download thread using the
											// other clients upload ports
									System.out.println("Downloading");
									System.out.println(reqlist.get(msg
											.get_integer()));
									System.out.println(downloadlist.get(msg
											.get_integer()));
									LinkedList<String> list = downloadlist
											.get(msg.get_integer());
									InFilethread = new ReceivingFile(
											list.get(0), list.get(1),
											msg.get_Inet(), msg.get_integer());
									InFilethread.start();
									down.put(msg.get_integer(), InFilethread);
									System.out.println("is this null "
											+ msg.get_integer() + ":  "
											+ InFilethread);

								}
								System.out.println("UPDATED!!!!!!!!");
								updateBox();
								break;
							case SEARCH:
								if (msg.get_integer() == searchTag - 1) {
									cp.add_message("+++++++ Start Search Results from "
											+ msg.get_username()
											+ " +++++ \n"
											+ msg.get_message()
											+ "+++++++ End of Search Results +++++");
									insertfiles(msg.get_username(),
											msg.get_message());
								}
								break;
							case REQSEARCH:
								boolean didYouMean = false;
								Iterator<String> keys = sharedFiles.keySet()
										.iterator();
								StringBuilder sb = new StringBuilder();
								while (keys.hasNext()) {
									String k = keys.next();
									// Comparing lower case to improve
									// search(Make search not case sensitive)
									String lowerK = k.toLowerCase();
									String[] array = msg.get_message().split(
											" ");
									for (String substring : array) {
										int difference = 0; // Difference
															// between words for
															// better search
															// results
										// Calculating

										String lowerSubstring = substring
												.toLowerCase();
										for (int i = 0; i < lowerSubstring
												.length(); i++) {
											if (lowerSubstring.charAt(i) != lowerK
													.charAt(i)) {
												difference++;
											}
										}
										System.out
												.println("Request search test");
										System.out.println("Comparing |" + k
												+ "| with |" + substring + "|");
										if (lowerK.contains(lowerSubstring)) {
											sb.append(k);
											sb.append("\n");
											break;
										} else if (difference <= 1) { // Otherwise
																		// finding
																		// difference
																		// between
																		// words
											if (!didYouMean) {
												sb.append("Did you mean:\n");
												didYouMean = true;
											}
											sb.append(k);
											sb.append("\n");
											break;
										}
									}
								}
								cp.add_message("You have been searched");
								Packet p = new Packet(Packet_Type.FSP,
										Packet_Subtype.SEARCH, username,
										msg.get_username(), sb.toString());
								p.set_integer(msg.get_integer());
								sendMessage(p);
								break;
							case FAILED:
								if (downloadlist.containsKey(msg.get_integer())) {
									cp.add_message("The user doesn't have that file");
									downloadlist.remove(msg.get_integer());
								} else {
									cp.add_message("Hmmm random failed request sent ...");
								}

								break;
							}
							break;
						default:
							System.out.println("\t outter ERORORORORO");
						}

					}
				} catch (IOException e) {

					System.exit(0);
				} catch (ClassNotFoundException e) {
					System.exit(0);
				}
			}
		}
	}

	/**
	 * Thread used to receive file from another client
	 * 
	 */
	class ReceivingFile implements Runnable {
		/**
		 * recv voice
		 */
		byte[] tempBuffer = new byte[BUFFERSIZE];
		Socket s;
		String sender;
		String filename;
		InetAddress in;
		Thread thisThread;
		int ID;

		public ReceivingFile(String sender, String filename, InetAddress in,
				int ID) {
			this.sender = sender;
			this.in = in;
			this.filename = filename;
			this.ID = ID;

		}

		public int getID() {
			return ID;
		}

		public void start() {
			thisThread = new Thread(this);
			thisThread.start();
		}

		public void stop() {
			thisThread = null;
		}

		public void run() {
			File f = new File("Downloading/" + filename);
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FileOutputStream FOS = null;
			try {
				FOS = new FileOutputStream(f);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ObjectInputStream ois = null;
			try {
				s = new Socket(in, 3001);
				ois = new ObjectInputStream(s.getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FileTransfer obj;
			boolean mustRun = true;
			;
			System.out.println("Attempting to recv");
			while (mustRun) {
				while (!pauseStatus) {
					try {
						// System.out.println("waiting for  bytes");
						obj = (FileTransfer) (ois.readObject());
						// System.out.println("got bytes");
						// System.out.println(obj.getPercentage() * 100);
						cp.update_progress(obj.getPercentage());
						FOS.write(obj.getFileBytes(), 0,
								obj.getFileBytes().length);
						//off += obj.getFileBytes().length;
						if (obj.getEOF()) {
							FOS.flush();
							System.out.println("Transfer completed");
							mustRun = false;
							break;
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			try {
				FOS.close();
				System.out.println("OutputStream closed successfully");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cp.add_message("Transfer completed of file");
			try {
				s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			f.renameTo(new File(SharedFolder + f.getName()));
			updateLocalFiles();
			cp.update_progress(0);
			uploading_inprogress = false;
			updateBox();
		}
	}

	/**
	 * Sends a buffered array of bytes to another client
	 * 
	 */
	class SendingFile implements Runnable {
		/**
		 * sending voice
		 */
		// byte[] tempBuffer = new byte[BUFFERSIZE];
		ServerSocket s;
		String receiver;
		String filename;
		boolean pause = false;
		boolean done = false;

		Thread thisThread;
		int ID;

		public SendingFile(String receiver, String filename, int ID) {
			this.receiver = receiver;
			this.filename = filename;
			this.ID = ID;
		}

		public int getID() {
			return ID;
		}

		public void start() {
			thisThread = new Thread(this);
			thisThread.start();
		}

		public void stop() {
			thisThread = null;
		}

		public void run() {
			ObjectOutputStream oos = null;
			Socket ac;
			try {
				s = new ServerSocket(3001);
				ac = s.accept();
				oos = new ObjectOutputStream(ac.getOutputStream());
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			File f = new File("Shared/" + filename);
			FileInputStream FIS = null;
			try {
				FIS = new FileInputStream(f);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int readBytes = 0;
			System.out.println("Attempting to read");
			int tempBytes = 0;
			while (!done) {
				while (!pauseStatus) {
					try {
						// System.out.println("start read");
						byte[] tempBuffer = new byte[BUFFERSIZE];
						tempBytes = FIS.read(tempBuffer);
						if (tempBytes != BUFFERSIZE) {
							byte[] tmp = new byte[tempBytes];
							for (int i = 0; i < tmp.length; i++) {
								tmp[i] = tempBuffer[i];
							}
							tempBuffer = tmp;
						}
						readBytes += tempBytes;
						// System.out.println("end read");
						if (readBytes == f.length()) {
							FileTransfer obj = new FileTransfer(tempBuffer,
									true);
							cp.update_progressU(((double) (readBytes))
									/ f.length());
							obj.setPercentage(((double) (readBytes))
									/ f.length());
							oos.writeObject(obj);
							done = true;
							break;
						} else {
							FileTransfer obj = new FileTransfer(tempBuffer,
									false);
							cp.update_progressU(((double) (readBytes))
									/ f.length());
							obj.setPercentage(((double) (readBytes))
									/ f.length());
							oos.writeObject(obj);

						}

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
			try {
				FIS.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cp.add_message("Transfer completed of file");
			try {
				s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cp.update_progressU(0);
			updateBox();
			uploading_inprogress = false;
		}
	}
}
