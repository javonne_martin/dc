import javax.swing.JFrame;

public class ClientEntry extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * @param args
	 */

	Login lp;
	ChatPanel cp;
	Username up;

	Client c;

	/**
	 * Constructor for client
	 */
	public ClientEntry() {
		this.setBounds(0, 0, 800, 550);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		c = new Client(this);
		lp = new Login(c);
		cp = new ChatPanel(c);
		up = new Username(c);
		c.initGUI();

		this.add(lp);
	}

	/**
	 * sets the login screen to the jframe
	 * 
	 * @return
	 */
	public boolean set_loginScreen() {

		this.add(lp);
		this.invalidate();
		this.validate();
		return true;
	}

	/**
	 * sets the username screen to the jframe
	 * 
	 * @return
	 */
	public boolean set_UsernameScreen() {
		this.remove(lp);
		this.add(up);
		this.invalidate();
		this.validate();

		return true;
	}

	/**
	 * sets the chat screen to the jframe
	 * 
	 * @return
	 */
	public boolean set_ChatScreen() {
		this.remove(up);
		this.add(cp);
		this.invalidate();
		this.validate();
		return true;
	}

	public Login get_login() {
		return lp;
	}

	public ChatPanel get_chatpanel() {
		return cp;
	}

	public Username get_userpanel() {
		return up;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ClientEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ClientEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ClientEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ClientEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new ClientEntry().setVisible(true);
			}
		});
	}

}
