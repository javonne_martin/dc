import java.io.Serializable;


public class FileTransfer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	boolean paused = false;
	boolean eof = false;
	byte[] bytes;
	double percentage = 0;
	public FileTransfer(byte[] bytes, boolean last){
		eof = last;
		this.bytes = bytes;
	}
	public byte[] getFileBytes(){
		return bytes;
	}
	public boolean getEOF(){
		return eof;
	}
	public void setPercentage(double p){
		percentage = p;
	}
	public double getPercentage(){
		return percentage;
	}
	public boolean getPauseStatus() {
		return paused;
	}
}
