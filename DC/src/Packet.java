import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;

public class Packet implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Packet_Type pt;
	Packet_Subtype pst;
	String para;
	String message;
	String username;
	Date d;
	int I;
	int O;
	InetAddress HostIP;
	int paraInt;
	public Packet(Packet_Type pt, Packet_Subtype pst,String username, String parameters,
			String message) {
		this.pt = pt;
		this.pst = pst;
		this.username = username;
		this.para = parameters;
		this.message = message;
	}
	public void set_integer(int i){
		paraInt = i;
	}
	public int get_integer(){
		return paraInt;
	}
	public void set_Inet(InetAddress in){
		this.HostIP = in;
	}
	public InetAddress get_Inet(){
		return HostIP;
	}
	public void set_userIn_udpsocket(int u){
		this.I = u;
	}
	public int get_userIn_udpsocket(){
		return I;
	}
	public void set_userOut_udpsocket(int r){
		this.O = r;
	}
	public int get_userOut_udpsocket(){
		return O;
	}
	public void set_date(Date d){
		this.d = d;
	}
	public Date get_date(){
		return d;
	}
	public Packet_Type get_packet_type(){
		return pt;
	}
	public Packet_Subtype get_packet_subtype(){
		return pst;
	}
	public String get_username(){
		return username;
	}
	public String get_parameters(){
		return para;
	}
	public String get_message(){
		return message;
	}
	public String toString(){
		return "Type :" +pt+" |Subtype :" +pst+" |Parameters :" +para+" |Message :" +message;
	}
}
