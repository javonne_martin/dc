import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Server {
	private ServerSocket socket;
	private Thread incoming_connections;
	private Thread msg_processor;
	boolean Online = false;

	Server thisServer = this;
	Map<String, User> clients;

	List<Packet> in_buffer;

	javax.swing.JTextArea mainf;
	javax.swing.JTextArea contactf;

	public Server(int port, javax.swing.JTextArea f1, javax.swing.JTextArea f2) {
		mainf = f1;
		contactf = f2;
		try {
			socket = new ServerSocket(port);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clients = Collections.synchronizedMap(new HashMap<String, User>());
		in_buffer = Collections.synchronizedList(new LinkedList<Packet>());

		Online = true;

		msg_processor = new Thread(new message_process());
		msg_processor.start();
		incoming_connections = new Thread(new incoming_user());
		incoming_connections.start();
	}

	public List<Packet> get_buffer() {
		return in_buffer;
	}

	public Map<String, User> get_clients() {
		return clients;
	}

	public void ResolvedClient(User u) {
		synchronized (clients) {
			clients.put(u.get_username(), u);
		}
	}

	class message_process implements Runnable {
		public void run() {
			while (Online) {
				synchronized (in_buffer) {
					if (in_buffer.size() > 0) {
						server_packet_processor(in_buffer.remove(0));
					}
				}
			}
		}

		public void server_packet_processor(Packet p) {
			Packet_Type pt = p.get_packet_type();
			Packet_Subtype pst = p.get_packet_subtype();
			p.set_date(new Date());
			switch (pt) {
			case COMMAND:
				switch (pst) {
				case USER_LIST:
					StringBuilder sb = new StringBuilder();
					synchronized (clients) {
						Iterator<String> ls = clients.keySet().iterator();
						while (ls.hasNext()) {
							sb.append(ls.next());
							if (ls.hasNext()) {
								sb.append(" ");
							}
						}
					}

					User u = clients.get(p.get_username());
					u.Send_Packet(new Packet(Packet_Type.COMMAND,
							Packet_Subtype.USER_LIST, p.get_username(), null,
							sb.toString()));
					break;

				}
				break;
			case CONNECTION:
				StringBuilder sb = new StringBuilder();
				switch (p.get_packet_subtype()) {
				case USER_JOINED:
					synchronized (clients) {
						Iterator<User> all_users = clients.values().iterator();
						Iterator<String> all_usersKey = clients.keySet()
								.iterator();
						while (all_usersKey.hasNext()) {
							System.out.println("Has next");
							sb.append(all_usersKey.next());
							if (all_usersKey.hasNext()) {
								sb.append("\n");
							}
						}
						String[] list = sb.toString().split("\n");
						Arrays.sort(list);
						sb = new StringBuilder();
						for (int s = 0; s < list.length; s++) {
							sb.append(list[s]);
							if (s != list.length - 1) {
								sb.append("\n");
							}
						}
						while (all_users.hasNext()) {
							User u = all_users.next();
							u.Send_Packet(p);
							u.Send_Packet(new Packet(Packet_Type.COMMAND,
									Packet_Subtype.USER_LIST, null, null, sb
											.toString()));
						}
					}
					contactf.setText(sb.toString());
					mainf.setText(mainf.getText() + "\n" + p.get_username()
							+ " joined the chat");
					break;
				case USER_DISCONNECTED:
					synchronized (clients) {
						Iterator<User> all_users = clients.values().iterator();
						Iterator<String> all_usersKey = clients.keySet()
								.iterator();
						while (all_usersKey.hasNext()) {
							System.out.println("Has next");
							sb.append(all_usersKey.next());
							if (all_usersKey.hasNext()) {
								sb.append("\n");
							}
						}
						while (all_users.hasNext()) {
							User u = all_users.next();
							u.Send_Packet(p);
							u.Send_Packet(new Packet(Packet_Type.COMMAND,
									Packet_Subtype.USER_LIST, null, null, sb
											.toString()));

						}
					}
					contactf.setText(sb.toString());
					mainf.setText(mainf.getText() + "\n" + p.get_username()
							+ " disconnected");
				}
				break;
			case MESSAGE:
				switch (pst) {
				case ALL:
					synchronized (clients) {
						Iterator<User> all_users = clients.values().iterator();
						while (all_users.hasNext()) {
							all_users.next().Send_Packet(p);
						}
					}
					mainf.setText(mainf.getText() + "\n" + p.get_username()
							+ ", " + p.get_date() + ": " + p.get_message());
					break;
				case WHISPER:
					synchronized (clients) {
						if (clients.containsKey(p.get_parameters())) {
							User ud = clients.get(p.get_parameters());
							User us = clients.get(p.get_username());
							ud.Send_Packet(p);
							us.Send_Packet(p);
							mainf.setText(mainf.getText() + "\n"
									+ "Whisper from " + p.get_username()
									+ " to " + p.get_parameters() + " : "
									+ p.get_message());

						} else {
							synchronized (in_buffer) {
								User us = clients.get(p.get_username());
								us.Send_Packet((new Packet(Packet_Type.MESSAGE,
										Packet_Subtype.FAILED,
										p.get_username(), p.get_parameters(),
										"Whisper Failed to "
												+ p.get_parameters()
												+ " User doesn't exisit")));
								mainf.setText(mainf.getText() + "\n"
										+ "Whisper from " + p.get_username()
										+ " to " + p.get_parameters()
										+ "Failed User doesn't exist");
							}
						}
					}
					break;
				}
				break;
			case FSP:
				switch (pst) {

				case REQFILE:
					System.out.println("1");
					synchronized (clients) {
						if (!clients.containsKey(p.get_parameters())) {
							synchronized (in_buffer) {
								in_buffer.add(new Packet(Packet_Type.FSP,
										Packet_Subtype.DECLINEREQ, p
												.get_username(), p
												.get_parameters(), "Call to "
												+ p.get_parameters()
												+ "Failed User doesn't exist"));
								mainf.setText(mainf.getText() + "\n"
										+ "Call to " + p.get_parameters()
										+ "Failed User doesn't exist");

							}
						} else {
							User u = clients.get(p.get_parameters());
							u.Send_Packet(p);
							mainf.setText(mainf.getText() + "\n"
									+ "User " + p.get_parameters()+" Requesting file from "+p.get_username()+" filename: "+p.get_message());
						}
					}
					break;

				case ACCEPTREQ:
					synchronized (clients) {
						User u = clients.get(p.get_parameters());
						u.Send_Packet(p);
						mainf.setText(mainf.getText() + "\n" + "Chat between "
								+ p.get_username() + " and "
								+ p.get_parameters() + " accepted");
					}

					break;

				case DECLINEREQ:
					synchronized (clients) {
						User u = clients.get(p.get_parameters());
						mainf.setText(mainf.getText() + "\n"
								+ "Declined chat between"
								+ (p.get_parameters()) + "and"
								+ p.get_username());
						u.Send_Packet(p);
					}
					break;

				case CLOSE:
					synchronized (clients) {
						if (clients.containsKey(p.get_parameters())) {
							User ud = clients.get(p.get_parameters());
							ud.Send_Packet(p);
						}
						User us = clients.get(p.get_username());
						mainf.setText(mainf.getText() + "\n"
								+ "Ending call between " + p.get_username()
								+ " and " + p.get_parameters());
						us.Send_Packet(p);
					}
					break;
				case EXCHANGEREQ:
					synchronized (clients) {
						User ud = clients.get(p.get_parameters());
						User us = clients.get(p.get_username());
						p.set_Inet(ud.get_Address());
						System.out.println(p.get_username() + " its ip is"
								+ ud.get_Address());
						System.out.println(p.get_parameters() + " its ip is"
								+ us.get_Address());
						System.out.println("Exchange to " + p.get_parameters());
						ud.Send_Packet(p);
					}
					break;
				case PAUSEFILE:
					break;
				case RESUMEFILE:
					break;
				case REQSEARCH:
					synchronized (clients) {
						Iterator<User> all_users = clients.values().iterator();
						while (all_users.hasNext()) {
							User u = all_users.next();
							if (!u.get_username().equals(p.get_username())) {
								u.Send_Packet(p);
							}
						}
					}
					break;
				case SEARCH:
					synchronized (clients) {
						if (clients.containsKey(p.get_parameters())) {
							User u = clients.get(p.get_parameters());
							u.Send_Packet(p);
						} else {
							System.out
									.println("Cant find the user who queried the search");
						}
					}
					break;
				case FAILED:
					synchronized (clients) {
						if (clients.containsKey(p.get_parameters())) {
							User u = clients.get(p.get_parameters());
							u.Send_Packet(p);
						} else {
							System.out
									.println("Cant find the user who queried the search");
						}
					}
					break;
				}
				break;
			}

		}
	}

	class incoming_user implements Runnable {
		@SuppressWarnings("unused")
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (Online) {
				try {
					Socket s = socket.accept();
					User u = new User(s, thisServer);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
