import javax.swing.JFrame;


public class ServerEntry extends JFrame{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
Server srv;
	public ServerEntry(){
		this.setBounds(0, 0, 800, 550);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ServerPanel sp = new ServerPanel();
		this.add(sp);
		srv = new Server(3000,sp.get_mainfield(),sp.get_userfield());
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ServerEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ServerEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ServerEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ServerEntry.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new ServerEntry().setVisible(true);
			}
		});
	}

}
