import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;
import java.util.Map;

public class User {
	private Socket s;
	private ObjectInputStream in;
	private ObjectOutputStream out;

	private boolean online = false;
	private String username;
	private Thread in_packets;
	Server server;

	User ThisUser = this;
	List<Packet> buffer;
	Map<String, User> clients;

	public User(Socket s, Server server) {
		this.s = s;
		this.server = server;

		try {
			in = new ObjectInputStream(s.getInputStream());
			out = new ObjectOutputStream(s.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clients = server.get_clients();
		buffer = server.get_buffer();

		online = true;
		in_packets = new Thread(new incoming_packets());
		in_packets.start();
	}

	public void Send_Packet(Packet msg) {
		try {
			out.writeObject(msg);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			shutdown();
		}
	}

	public InetAddress get_Address() {
		return s.getInetAddress();
	}

	public String get_username() {
		return username;
	}

	public void shutdown() {
		synchronized (clients) {
			if (clients.containsValue(ThisUser)) {
				clients.remove(username);
			}
		}
		online = false;
	}

	class incoming_packets implements Runnable {
		public void run() {
			Packet p;
			while (online) {
				try {
					if ((p = (Packet) in.readObject()) != null) {
						packet_process(p);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					shutdown();
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					shutdown();

				}
			}
		}

		public void packet_process(Packet p) {
			switch (p.get_packet_type()) {
			case CONNECTION:
				switch (p.get_packet_subtype()) {
				case USER_DISCONNECTED:

					shutdown();
					synchronized (buffer) {
						System.out.println("Sending to server");
						buffer.add(p);
					}
					break;
				case VALIDATE_NAME:
					synchronized (clients) {
						try {
							if (!clients.containsKey(p.get_parameters())) {
								username = p.get_parameters();
								server.ResolvedClient(ThisUser);

								out.writeObject(new Packet(
										Packet_Type.CONNECTION,
										Packet_Subtype.VALIDATE_NAME, username,
										username, "true"));
								System.out.println(username
										+ ": has been added to the list");
								synchronized (buffer) {
									buffer.add(new Packet(
											Packet_Type.CONNECTION,
											Packet_Subtype.USER_JOINED,
											username, username, null));
								}
							} else {

								out.writeObject(new Packet(
										Packet_Type.CONNECTION,
										Packet_Subtype.VALIDATE_NAME, null,
										username, "false"));

							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					break;
				}
				break;
			default:
				synchronized (buffer) {
					System.out.println("Sending to server");
					buffer.add(p);
				}
				break;
			}
		}
	}
}
