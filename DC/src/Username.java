/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author jonny91289
 */
public class Username extends javax.swing.JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Creates new form Username
     */
	Client c;
    public Username(Client c) {
        initComponents();
        this.c = c;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        uUsername_input = new javax.swing.JTextField();
        uUsername = new javax.swing.JLabel();
        uInstruct1 = new javax.swing.JLabel();
        uJoin = new javax.swing.JButton();
        uOutput = new javax.swing.JLabel();

        uUsername.setText("Username");

        uInstruct1.setText("Please Complete the fields");

        uJoin.setText("Join");
        uJoin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uJoinActionPerformed(evt);
            }
        });

        uOutput.setText("Please Join the chat");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(210, 210, 210)
                        .addComponent(uUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(uUsername_input, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(294, 294, 294)
                        .addComponent(uInstruct1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(301, 301, 301)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(uOutput)
                            .addComponent(uJoin, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(261, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addComponent(uInstruct1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uUsername)
                    .addComponent(uUsername_input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 260, Short.MAX_VALUE)
                .addComponent(uOutput)
                .addGap(18, 18, 18)
                .addComponent(uJoin)
                .addGap(49, 49, 49))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void uJoinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uJoinActionPerformed
        //TODO Join 
    	if(uUsername_input.getText() == null){
    		return;
    	}
    	if(uUsername_input.getText().equals("")){
    		return;
    	}
    	if(uUsername_input.getText().contains(" ")){
    		return;
    	}
    	c.Login(uUsername_input.getText());
        // TODO add your handling code here:
    }//GEN-LAST:event_uJoinActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel uInstruct1;
    private javax.swing.JButton uJoin;
     javax.swing.JLabel uOutput;
    private javax.swing.JLabel uUsername;
    private javax.swing.JTextField uUsername_input;
    // End of variables declaration//GEN-END:variables
}
